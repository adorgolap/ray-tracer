#ifndef _CYLINDER
#define _CYLINDER
class Cylinder : public Object
{
public:
	double xCenter;
	double zCenter;
	double radius;
	double yMin;
	double yMax;

};
#endif