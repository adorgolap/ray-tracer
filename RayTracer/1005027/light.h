#ifndef _LIGHT
#define _LIGHT
#include<glut.h>
class Light{
	public:
		double x,y,z;
		Light(){
			x = y = z = 0;
		}
		Light(double xx,double yy, double zz)
		{
			x = xx;
			y = yy;
			z = zz;
		}
};
void enableLighting()
{
	glEnable ( GL_LIGHTING ) ;
	glShadeModel(GL_SMOOTH);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
}
#endif