#ifndef _SPHERE
#define _SPHERE
#include"object.h"
class Sphere : public Object
{
	public:
		Point center;
		double radius;
};
#endif