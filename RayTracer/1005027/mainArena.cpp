#include<stdio.h>

#include<stdlib.h>
#include<math.h>
#include "camera.h"
#include "listener.h"
#include "light.h"
#include "objectreader.h"
#include "ifta.h"
#include "object.h"
#include "sphere.h"
#include "cylinder.h"
#include "triangle.h"
#include "checkerboard.h"
#include "drawing.h"
#define GRID_SIZE 10

void readSpecs();




void display(){
	clearDisplay();
	setupCamera();
	//code here
	drawSpheres(spheres,numberOfSpCyTr[0]);
	drawCylinders(cylinders,numberOfSpCyTr[1]);
	drawTriangles(triangles,numberOfSpCyTr[2]);
	drawCheckerBoard(checkerBoard);
	//drawDebug();
	//drawGrid(GRID_SIZE,CAMERA_DISTANCE,canDrawGrid);
	glutSwapBuffers();
}

void animate(){
	//codes for any changes in Camera
	glutPostRedisplay();	//this will call the display AGAIN
}




void init(){
	//codes for initialization
	initializeCamera();
	canDrawGrid = true;
	//clear the screen
	glClearColor(BLACK, 0);
	//lighting
	
	enableLighting();
	
	//initialize the matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(70,	1,	0.1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
	loadCameraData();
	readSpecs();
	
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	pixels = readPixels();
	printf("pixels %d\n",pixels);
	glutInitWindowSize(pixels	, pixels);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("Ray tracer 1005027");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	//ADD keyboard listeners:
	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);

	//ADD mouse listeners:
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}

void readSpecs()
{
	reccursionDepth = readRecDepth();
	printf("rec depth %d\n",reccursionDepth);
	//get number of objects
	numberOfSpCyTr = getNumberOfSphereCylinderTriangle();
	printf("Sphere %d\nCylinder %d\nTriangle %d\n",numberOfSpCyTr[0],numberOfSpCyTr[1],numberOfSpCyTr[2]);

	//read spheres
	spheres = readSpheres(numberOfSpCyTr[0]);
	for(int i = 0 ; i < numberOfSpCyTr[0];i++)
	{
		printf("radius %lf\n",spheres[i].radius);
		printf("center %lf,%lf,%lf\n",spheres[i].center.x,spheres[i].center.y,spheres[i].center.z);
		printf("color %d,%d,%d\n",spheres[i].color.x,spheres[i].color.y,spheres[i].color.z);
		printf("a %lf\nd %lf\nr %lf\nsp %lf\np %lf\nref %lf\n\n",spheres[i].ambientCoefficient,spheres[i].diffiuseCoefficient,spheres[i].reflectionCoefficient,spheres[i].specularCoefficient,spheres[i].phongCoefficient,spheres[i].refractiveIndex);
	}

	//read cylinders
	cylinders = readCylinders(numberOfSpCyTr[1]);
	for(int i = 0 ; i < numberOfSpCyTr[1];i++)
	{
		printf("radius %lf\n",cylinders[i].radius);
		printf("xCenter %lf\nzCenter %lf\nyMin %lf\nyMax %lf\n",cylinders[i].xCenter,cylinders[i].zCenter,cylinders[i].yMin,cylinders[i].yMax);
		printf("color %d,%d,%d\n",cylinders[i].color.x,cylinders[i].color.y,cylinders[i].color.z);
		printf("a %lf\nd %lf\nr %lf\nsp %lf\np %lf\nref %lf\n\n",cylinders[i].ambientCoefficient,cylinders[i].diffiuseCoefficient,cylinders[i].reflectionCoefficient,cylinders[i].specularCoefficient,cylinders[i].phongCoefficient,cylinders[i].refractiveIndex);
	}

	//read triangles
	triangles = readTriangles(numberOfSpCyTr[2]);
	for(int i = 0 ; i < numberOfSpCyTr[1];i++)
	{
		printf("A %lf,%lf,%lf\n",triangles[i].A.x,triangles[i].A.y,triangles[i].A.z);
		printf("B %lf,%lf,%lf\n",triangles[i].B.x,triangles[i].B.y,triangles[i].B.z);
		printf("C %lf,%lf,%lf\n",triangles[i].C.x,triangles[i].C.y,triangles[i].C.z);
		printf("color %d,%d,%d\n",triangles[i].color.x,triangles[i].color.y,triangles[i].color.z);
		printf("a %lf\nd %lf\nr %lf\nsp %lf\np %lf\nref %lf\n\n",triangles[i].ambientCoefficient,triangles[i].diffiuseCoefficient,triangles[i].reflectionCoefficient,triangles[i].specularCoefficient,triangles[i].phongCoefficient,triangles[i].refractiveIndex);
	}
	checkerBoard = readCheckerBoard();

	numberOfLight = getNumberOfLights();
	printf("nol %d\n",numberOfLight);
	lightPositions = readLightPositions(numberOfLight);
	for(int i = 0 ; i < numberOfLight ;i++)
	{
		printf("L %lf, %lf, %lf\n\n",lightPositions[i].x,lightPositions[i].y,lightPositions[i].z);
	}

}