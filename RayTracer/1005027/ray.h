#pragma once
#include "vector.h"
#include"point.h"
#include<math.h>

class Ray{
	public:
		Point O;
		Vector d;
		Ray();
		Ray(Point,Vector);
		Ray(Point,Point);
};

Ray::Ray()
{
	O = Point();
	d = Vector();
	d.normalize();
}
Ray::Ray(Point o,Vector dd)
{
	O= o;
	d = dd;
	d.normalize();
}
Ray::Ray(Point start, Point passingThrough)
{
	O = start;
	d = Vector(start,passingThrough);
	d.normalize();
}