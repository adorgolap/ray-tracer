#ifndef _OBJECT
#define _OBJECT
#include "ifta.h"
class Object{
	public:
		Color color;
		double ambientCoefficient;
		double diffiuseCoefficient;
		double specularCoefficient;
		double phongCoefficient;
		double reflectionCoefficient;
		double refractiveIndex;

};
#endif