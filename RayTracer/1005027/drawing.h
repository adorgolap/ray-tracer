#ifndef _DRAWING
#define _DRAWING
#include "sphere.h"
#include"cylinder.h"
#include"triangle.h"
#include"checkerboard.h"
#include"values.h"
#include"ifta.h"
void drawSingleCylinder(Cylinder c)
{
	Point baseCenter = Point(c.xCenter,c.zCenter,c.yMin);
	drawCylinder(baseCenter,c.radius,c.radius,(c.yMax-c.yMin),c.color.x,c.color.y,c.color.z,SOLID);
}
void drawSpheres(Sphere* s, int numberOfSpheres)
{
	for(int i = 0 ; i < numberOfSpheres; i++)
	{
		drawSphere(s[i].center,s[i].radius,s[i].color.x,s[i].color.y,s[i].color.z,SOLID);
	}
}
void drawCylinders(Cylinder* cylinders, int numberOfCylinders)
{
	for(int i = 0 ; i < numberOfCylinders;i++)
	{
		drawSingleCylinder(cylinders[i]);
	}
}
void drawTriangles(Triangle* triangles,int numberOfTriangles)
{
	for(int i = 0 ; i < numberOfTriangles;i++)
	{
		drawTriangle(triangles[i].A,triangles[i].B,triangles[i].C,triangles[i].color);
	}
}
void drawCheckerBoard(CheckerBoard cb)
{
	Point p ;
	int toogleColor = 0;
	for(int i = -cb.range; i < cb.range+cb.size; i+=cb.size)
	{
		for(int j = -cb.range; j<cb.range+cb.size;j+=cb.size)
		{
			p = Point(i,j,0);
			if(toogleColor){
				drawRectangle(p,20,20,cb.color);
				toogleColor = 0;
			}else{
				drawRectangle(p,20,20,cb.additionalColor);
				toogleColor = 1;
			}
		}
	}
}
/*void drawDebug()
{
	
	drawSphere(eye,3,MAGENTA,SOLID);
	drawSphere(lookAt,3,LIGHT_WOOD,SOLID);
	/*if(done)
	{
		for(int i = 0 ; i < pixels*pixels;i++)
		{
			if(i%2)
			{
				drawSphere(debug[i],0.1,ORANGE,SOLID);
			}else
			{
				drawSphere(debug[i],0.1,BLUE,SOLID);
			}
		}
	}
}*/

#endif