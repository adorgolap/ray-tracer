#ifndef _VECTOR
#define _VECTOR
#include "ifta.h"
#include "point.h"
#include<math.h>
class Vector
{
	public:
		double i,j,k;
		Vector();
		Vector(double,double,double);
		Vector(Point start,Point end)
		{
			i = end.x - start.x;
			j = end.y - start.y;
			k = end.z - start.z;
			/*double m = sqrt((i*i) + (j*j) + (k*k));
			i = i/m;
			j = j/m;
			k = k/m;*/
		}
		double magnitude()
		{
			return sqrt(i*i+j*j+k*k);
		}
		void normalize()
		{
			double m = sqrt((i*i) + (j*j) + (k*k));
			i = i/m;
			j = j/m;
			k = k/m;
		}
		Vector negative()
		{
			return Vector(-i,-j,-k);
		}
		double dotProduct(Vector v)
		{
			return i*v.i + j*v.j + k*v.k;
		}
		Vector crossProduct(Vector v)
		{
			return Vector(j*v.k-k*v.j,k*v.i-i*v.k,i*v.j-j*v.i);
		}
		Vector add(Vector v)
		{
			return Vector(i+v.i,j+v.j,k+v.k);
		}
		Vector multiply(double scale)
		{
			return Vector(i*scale,j*scale,k*scale);
		}
		Vector minus(Vector v)
		{
			return Vector(i-v.i,j-v.j,k-v.k);
		}
};
Vector::Vector()
{
	i = j = k = 1;
}
Vector::Vector(double i,double j , double k)
{
	this->i = i;
	this->j = j;
	this->k = k;
}
#endif
