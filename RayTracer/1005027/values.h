#ifndef _VALUES
#define _VALUES
#include"sphere.h"
#include"cylinder.h"
#include"triangle.h"
#include"checkerboard.h"
#include"light.h"
#define ONE_DEGREE 0.01745329252
#define TWO_DEGREE 0.03490658504
#define FIVE_DEGREE 0.087266
#define TEN_DEGREE 0.1745329
#define FOURTY_FIVE_DEGREE 0.7853981634
#define NINETEY_DEGREE 1.570796327
#define ONE_EIGHTY_DEGREE 3.14159254
#define INFINITY 1000
#define SPHERE 0
#define CYLINDER 1
#define TRIANGLE 2
#define EPSILON 0.5
bool canDrawGrid = true;

int reccursionDepth = 1;
int pixels = 512;

int *numberOfSpCyTr;
Sphere* spheres;
Cylinder* cylinders;
Triangle* triangles;
CheckerBoard checkerBoard;
Light* lightPositions;
int numberOfLight;
Point *debug;

//Point eye = Point(200,0,0);
//Point lookAt = Point(0,0,0);

int done = 0;
#endif