#ifndef _RAY
#define _RAY
#include<stdio.h>
#include<cstdlib>
#include<stdlib.h>
#include<math.h>
#include<iostream>
#include"camera.h"
#include"values.h"
#include"vector.h"
#include"point.h"
#include"ray.h"
#include"image.h"
#include"sphere.h"
#include"cylinder.h"

using namespace std;
Color trace(Ray r,int depth);

Point getPoint(Point origin,Vector direction,double distance)
{
	Point p = Point();
	p.x = origin.x+ direction.i*distance; 
	p.y = origin.y+ direction.j*distance;
	p.z = origin.z+ direction.k*distance;
	return p;

}
void printVector(Vector v)
{
	cout << v.i << ' ' << v.j << ' ' << v.k << endl;
}
void printPoint(const char * msg, Point p)
{
	printf("%s ",msg);
	printf("%g, %g, %g\n",p.x,p.y,p.z);
}
void printRay(Ray r)
{
	printPoint("origin",r.O);
	
}
double intersectsWith(Ray r, Sphere s)
{
	//Solve At^2 + Bt + c = 0 for t
	//for sphere
	//A = 1
	//B = 2|d|*|O-C|
	//C = |O-C|^2 - r^2
	//d is the direction of ray
	//O is the origin of ray
	//C is the center od sphere
	//r is the radius of sphere
	//t is the distance of intersection
	//return the smaller t as it indicates front intersection

	//printVector(r.d);

	Vector OC = Vector( s.center,r.O);
	double A = 1;
	double B = 2*r.d.dotProduct(OC);
	double C = OC.magnitude()*OC.magnitude() - s.radius*s.radius;
	double determinant = B*B -4*A*C;
	if(determinant < 0)
	{
		return - 1;
	}
	double t1 = (-B+sqrt(determinant))/(2*A);
	double t2 = (-B-sqrt(determinant))/(2*A);
	if(t1 < t2)
	{	if(t1 < 0)
		{
			return -1;
		}
		return t1;
	}
	if(t2 < 0)
	{
		return -1;
	}
	return t2;
}
double intersectsWith(Ray r, Cylinder c)
{

	//check for upper lid
	double ut = -1;
	if(r.d.k != 0)
	{
		double temp_t = (c.yMax - r.O.z)/r.d.k;
		Point P = getPoint(r.O,r.d,temp_t);
		Point C = Point(c.xCenter,c.zCenter,c.yMax);
		Vector PC = Vector(C,P);
		double magnitude = PC.magnitude();
		if(magnitude <  c.radius)
		{
			ut = temp_t;
		}
	}
	//check for lowr lid
	double lt = -1;
	if(r.d.k != 0)
	{
		double temp_t = (c.yMin - r.O.z)/r.d.k;
		Point P = getPoint(r.O,r.d,temp_t);
		Point C = Point(c.xCenter,c.zCenter,c.yMin);
		Vector PC = Vector(C,P);
		double magnitude = PC.magnitude();
		if(magnitude <  c.radius)
		{
			lt = temp_t;
		}
	}
	//check for curved surface
	//kx = Ox - Cx
	//ky = Oy - Cy
	//A = dx^2 + dy^2
	//B = 2kxdk+2kydy 
	//C = kx^2 + ky^2 - r^2
	//solve At^2 + Bt + C = 0 for t
	//Find Pz = Oz + tdz
	//if Pz is within range then intersect
	double kx = r.O.x - c.xCenter;
	double ky = r.O.y - c.zCenter;
	double A = r.d.i*r.d.i + r.d.j*r.d.j;
	double B = 2*kx*r.d.i + 2*ky*r.d.j;
	double C = kx*kx + ky*ky - c.radius*c.radius;
	double determinant = B*B -4*A*C;
	double t = -1;
	if(determinant < 0)
	{
		t =  -1;
	}else{
		double t1 = (-B+sqrt(determinant))/(2*A);
		double t2 = (-B-sqrt(determinant))/(2*A);
		if(t1<t2)
		{
			t =  t1;
		}else
		{
			t = t2;
		}
	
		double pz = r.O.z + t*r.d.k;
		if(c.yMax >= pz && c.yMin <= pz)
		{
		}else
		{
			t = -1;
		}
	}
	if(t == -1 && ut == -1 && lt ==-1)
	{
		return -1;
	}else
	{
		if(t == -1)
		{
			t = INFINITY*INFINITY;
		}
		if(ut == -1)
		{
			ut = INFINITY*INFINITY;
		}
		if(lt == -1)
		{
			lt = INFINITY*INFINITY;
		}
		if(t <= ut && t <= lt)
		{
			if(t < 0)
			{
				return -1;
			}
			return t;
		}
		if(ut <= t && ut <=lt)
		{
			if(ut < 0)
			{
				return -1;
			}
			return ut;
		}
		if(lt <= t && lt <=ut)
		{
			if(lt < 0)
			{
				return -1;
			}
			return lt;
		}
	}
	return -1;
}
double intersectsWith(Ray r, Triangle tr)
{
	Vector AB = Vector(tr.A,tr.B);
	Vector AC = Vector(tr.A,tr.C);
	Vector n = AB.crossProduct(AC);
	n.normalize();
	Point Po = tr.A;
	Vector OPo = Vector(r.O,Po);
	double t = OPo.dotProduct(n)/r.d.dotProduct(n);

	Point P = getPoint(r.O,r.d,t);
	Vector BC = Vector(tr.B,tr.C);
	Vector CA = Vector(tr.C,tr.A);
	Vector AP = Vector(tr.A,P);
	Vector BP = Vector(tr.B,P);
	Vector CP = Vector(tr.C,P);

	double moment1 = (AB.crossProduct(AP)).dotProduct(n);
	double moment2 = (BC.crossProduct(BP)).dotProduct(n);
	double moment3 = (CA.crossProduct(CP)).dotProduct(n);

	if(moment1 >= 0 && moment2 >= 0 && moment3 >= 0)
	{
		if(t < 0)
		{
			return -1;
		}
		return t;
	}
	if(moment1 <= 0 && moment2 <= 0 && moment3 <= 0)
	{
		if(t < 0)
		{
			return -1;
		}
		return t;
	}
	return -1;
}

double intersectsWith(Ray r, CheckerBoard cb)
{
	Point A = Point(0,0,0);
	Point B = Point(cb.size,0,0);
	Point C = Point(0,cb.size,0);

	Vector AB = Vector(A,B);
	Vector AC = Vector(A,C);
	Vector n = AB.crossProduct(AC);
	n.normalize();

	Point Po = A;
	Vector OPo = Vector(r.O,Po);
	if(r.d.dotProduct(n) <= 0)
	{
		double t = OPo.dotProduct(n)/r.d.dotProduct(n);
		if(t < 0)
		{
			return -1;
		}
		return t;
	}
	return -1;
}
int intersectsWithAnyObject(Ray r)
{
	int nS = numberOfSpCyTr[SPHERE],nC= numberOfSpCyTr[CYLINDER],nT= numberOfSpCyTr[TRIANGLE];
	double minDistance = INFINITY;
	int closestObject = -1;
	int indexOfClosestSphere = -1;
	int indexOfClosestCylinder = -1;
	int indexOfClosestTrianlge = -1;
	//check for sphere
	for(int i = 0 ; i < nS; i++)
	{
		double distance =intersectsWith (r,spheres[i]);
		if(distance != -1)
		{
			if(distance < minDistance)
			{
				minDistance = distance;
				indexOfClosestSphere = i;
				closestObject = SPHERE;
			}
		}
	}
	//check for cylinder
	for(int i = 0 ; i < nC; i++)
	{
		double distance = intersectsWith(r,cylinders[i]);
		if(distance != -1)
		{
			//cout<<distance<<endl;
			if(distance < minDistance)
			{
				minDistance = distance;
				indexOfClosestCylinder = i;
				closestObject = CYLINDER;
			}
		}
	}
	//check with triangle
	for(int i = 0 ; i < nT; i++)
	{
		double distance = intersectsWith(r,triangles[i]);
		if(distance != -1)
		{
			if(distance < minDistance)
			{
				minDistance = distance;
				indexOfClosestTrianlge = i;
				closestObject = TRIANGLE;
			}
		}
	}
	double distance = intersectsWith(r,checkerBoard);
	if(closestObject == -1 && distance == -1)
	{
		return 0;
	}
	return 1;
}
Color getDiffiuseColor(Sphere s,Ray r, double t)
{
	Color color = Color(0,0,0);
	Point P = getPoint(r.O,r.d,t);
	Vector n = Vector(s.center,P);
	n.normalize();
	for(int i = 0 ; i < numberOfLight ; i++)
	{
		Point L = Point(lightPositions[i].x,lightPositions[i].x,lightPositions[i].z);
		Vector PL = Vector(P,L);
		PL.normalize();
		//check for blocking object
		Point EP = getPoint(P,PL,EPSILON);
		Ray checkingRay = Ray(EP,PL);
		int hasBlocking = intersectsWithAnyObject(checkingRay);
		if(hasBlocking == 0)
		{
			double cosTheta = PL.dotProduct(n);
			if(cosTheta > 0)
			{
				color.x += s.color.x*s.diffiuseCoefficient*cosTheta;
				color.y += s.color.y*s.diffiuseCoefficient*cosTheta;
				color.z += s.color.z*s.diffiuseCoefficient*cosTheta;
			}
		}
	}
	color.x = color.x/numberOfLight;
	color.y = color.y/numberOfLight;
	color.z = color.z/numberOfLight;
	
	return color;
}
Color getDiffiuseColor(Cylinder c,Ray r, double t)
{
	Color color = Color(0,0,0);
	Point P = getPoint(r.O,r.d,t);
	Point C = Point(c.xCenter,c.zCenter,P.z);
	Vector n = Vector(C,P);
	n.normalize();
	for(int i = 0 ; i < numberOfLight ; i++)
	{
		Point L = Point(lightPositions[i].x,lightPositions[i].x,lightPositions[i].z);
		Vector PL = Vector(P,L);
		PL.normalize();
		Point EP = getPoint(P,PL,EPSILON);
		Ray checkingRay = Ray(EP,PL);
		int hasBlocking = intersectsWithAnyObject(checkingRay);
		if(hasBlocking == 0)
		{
			double cosTheta = PL.dotProduct(n);
			if(cosTheta <0)
			{
				cosTheta = 0;
			}
			color.x += c.color.x*c.diffiuseCoefficient*cosTheta;
			color.y += c.color.y*c.diffiuseCoefficient*cosTheta;
			color.z += c.color.z*c.diffiuseCoefficient*cosTheta;
		}
	}
	color.x = color.x/numberOfLight;
	color.y = color.y/numberOfLight;
	color.z = color.z/numberOfLight;
	return color;
}
Color getDiffiuseColor(Triangle tr,Ray r, double t)
{
	Color color = Color(0,0,0);
	Vector AC = Vector(tr.A,tr.C);
	Vector AB = Vector(tr.A,tr.B);
	Vector n1 = AC.crossProduct(AB);
	Vector n2 = AB.crossProduct(AC);
	n1.normalize();
	n2.normalize();
	Point P = getPoint(r.O,r.d,t);
	for(int i = 0 ; i < numberOfLight ; i++)
	{
		Point L = Point(lightPositions[i].x,lightPositions[i].x,lightPositions[i].z);
		Vector PL = Vector(P,L);
		PL.normalize();
		Point EP = getPoint(P,PL,EPSILON);
		Ray checkingRay = Ray(EP,PL);
		int hasBlocking = intersectsWithAnyObject(checkingRay);
		if(hasBlocking == 0)
		{
			double c1 = n1.dotProduct(PL);
			double c2 = n2.dotProduct(PL);
			Vector n;
			if(c1 < 0)
			{
				n = n2;
			}
			else
			{
				n = n1;
			}
			double cosTheta = PL.dotProduct(n);
			if(cosTheta <0)
			{
				cosTheta = 0;
			}
			color.x += tr.color.x*tr.diffiuseCoefficient*cosTheta;
			color.y += tr.color.y*tr.diffiuseCoefficient*cosTheta;
			color.z += tr.color.z*tr.diffiuseCoefficient*cosTheta;
		}
	}
	color.x = color.x/numberOfLight;
	color.y = color.y/numberOfLight;
	color.z = color.z/numberOfLight;
	return color;
}
Color getSpecularColor(Sphere s,Ray r, double t)
{
	Color color = Color(0,0,0);
	Point P = getPoint(r.O,r.d,t);
	Vector n = Vector(P,s.center);
	n = n.negative();
	n.normalize();
	for(int i = 0 ; i < numberOfLight ;i++)
	{
		Point L = Point(lightPositions[i].x,lightPositions[i].x,lightPositions[i].z);
		Vector PL = Vector(P,L);
		PL.normalize();
		Point EP = getPoint(P,PL,EPSILON);
		Ray checkingRay = Ray(EP,PL);
		int hasBlocking = intersectsWithAnyObject(checkingRay);
		if(hasBlocking == 0)
		{
			Vector LP = Vector(L,P);
			Vector PR = LP.minus(n.multiply(2*(LP.dotProduct(n))));
			PR.normalize();
			Vector PV = Vector(P,r.O);
			PV.normalize();
			double cosTheta = PV.dotProduct(PR);
			//LP.normalize();
			//double cosTheta = LP.dotProduct(PR);
			if(cosTheta <0)
			{
				cosTheta = 0;
			}else{
				cosTheta = pow(cosTheta,s.phongCoefficient);
				color.x += s.color.x*cosTheta*s.specularCoefficient;
				color.y += s.color.y*cosTheta*s.specularCoefficient;
				color.z += s.color.z*cosTheta*s.specularCoefficient;
			}
		}
	}
	color.x = color.x/numberOfLight;
	color.y = color.y/numberOfLight;
	color.z = color.z/numberOfLight;
	return color;
}
Color getSpecularColor(Cylinder c,Ray r, double t)
{
	Color color = Color(0,0,0);
	Point P = getPoint(r.O,r.d,t);
	Point C = Point(c.xCenter,c.zCenter,P.z);
	Vector n = Vector(C,P);
	n.normalize();
	for(int i = 0 ; i < numberOfLight ;i++)
	{
		Point L = Point(lightPositions[i].x,lightPositions[i].x,lightPositions[i].z);
		Vector PL = Vector(P,L);
		PL.normalize();
		Point EP = getPoint(P,PL,EPSILON);
		Ray checkingRay = Ray(EP,PL);
		int hasBlocking = intersectsWithAnyObject(checkingRay);
		if(hasBlocking == 0)
		{
			Vector LP = Vector(L,P);
			Vector PR = LP.minus(n.multiply(2*(LP.dotProduct(n))));
			PR.normalize();
			Vector PV = Vector(P,r.O);
			PV.normalize();
			double cosTheta = PV.dotProduct(PR);
			//LP.normalize();
			//double cosTheta = LP.dotProduct(PR);
			if(cosTheta <0)
			{
				cosTheta = 0;
			}else
			{
				cosTheta = pow(cosTheta,c.phongCoefficient);
				color.x += c.color.x*cosTheta*c.specularCoefficient;
				color.y += c.color.y*cosTheta*c.specularCoefficient;
				color.z += c.color.z*cosTheta*c.specularCoefficient;
			}
		}
	}
	color.x = color.x/numberOfLight;
	color.y = color.y/numberOfLight;
	color.z = color.z/numberOfLight;
	return color;
}
Color getSpecularColor(Triangle tr,Ray r, double t)
{
	Color color = Color(0,0,0);
	Vector AC = Vector(tr.A,tr.C);
	Vector AB = Vector(tr.A,tr.B);
	Vector n1 = AC.crossProduct(AB);
	Vector n2 = AB.crossProduct(AC);
	n1.normalize();
	n2.normalize();
	Point P = getPoint(r.O,r.d,t);
	for(int i = 0 ; i < numberOfLight ;i++)
	{

		Point L = Point(lightPositions[i].x,lightPositions[i].x,lightPositions[i].z);
		Vector PL = Vector(P,L);
		PL.normalize();
		Point EP = getPoint(P,PL,EPSILON);
		Ray checkingRay = Ray(EP,PL);
		int hasBlocking = intersectsWithAnyObject(checkingRay);
		if(hasBlocking == 0)
		{
			double c1 = n1.dotProduct(PL);
			double c2 = n2.dotProduct(PL);
			Vector n;
			if(c1 < 0)
			{
				n = n2;
			}
			else
			{
				n = n1;
			}
			Vector LP = Vector(L,P);
			Vector PR = LP.minus(n.multiply(2*(LP.dotProduct(n))));
			PR.normalize();
			Vector PV = Vector(P,r.O);
			PV.normalize();
			double cosTheta = PV.dotProduct(PR);
			if(cosTheta <0)
			{
				cosTheta = 0;
			}else
			{
				cosTheta = pow(cosTheta,tr.phongCoefficient);
				color.x += tr.color.x*cosTheta*tr.specularCoefficient;
				color.y += tr.color.y*cosTheta*tr.specularCoefficient;
				color.z += tr.color.z*cosTheta*tr.specularCoefficient;
			}
		}
	}
	color.x = color.x/numberOfLight;
	color.y = color.y/numberOfLight;
	color.z = color.z/numberOfLight;
	return color;
}
Color getAmbientColor(Sphere s)
{
	Color color = Color(s.color.x*s.ambientCoefficient,s.color.y*s.ambientCoefficient,s.color.z*s.ambientCoefficient);
	return color;
}
Color getAmbientColor(Cylinder s)
{
	Color color = Color(s.color.x*s.ambientCoefficient,s.color.y*s.ambientCoefficient,s.color.z*s.ambientCoefficient);
	return color;
}
Color getAmbientColor(Triangle tr)
{
	Color color = tr.color;
	color.multiply(tr.ambientCoefficient);
	return color;
}
Color getPhongLightColor(Sphere s,Ray r, double t)
{
	Color d = getDiffiuseColor(s,r,t);
	Color a = getAmbientColor(s);
	//cout<<d.x<<' '<<d.y<<' '<<d.z<<endl;
	Color sp = getSpecularColor(s,r,t);
	Color f = a;
	f.plus(d);
	f.plus(sp);
	//Color c = s.color;
	//c.multiply((1-s.reflectionCoefficient));
	return f;
}
Color getPhongLightColor(Cylinder c,Ray r, double t)
{
	Color a = getAmbientColor(c);
	Color d = getDiffiuseColor(c,r,t);
	Color sp = getSpecularColor(c,r,t);
	Color f = a;
	f.plus(d);
	f.plus(sp);
	return f;
}
Color getPhongLightColor(Triangle tr,Ray r, double t)
{
	Color d = getDiffiuseColor(tr,r,t);
	Color a = getAmbientColor(tr);
	//cout<<a.x<<' '<<a.y<<' '<<a.z<<endl;
	Color sp = getSpecularColor(tr,r,t);
	//Color f = Color(a.x+d.x+sp.x,a.y+d.y+sp.y,a.z+d.z+sp.z);
	Color f = a;
	f.plus(d);
	f.plus(sp);
	return f;
}

Color getColorAtDistance(Ray r,double t)
{
	if(t == -1)
	{
		return Color(0,0,0);
	}
	if(t > checkerBoard.range || t < -checkerBoard.range)
	{
		return Color(0,0,0);
	}
	Point P = getPoint(r.O,r.d,t);
	int x = (int)(P.x+checkerBoard.range) / checkerBoard.size;
	int y = (int)(P.y+checkerBoard.range) / checkerBoard.size;
	if((x+y)%2 == 0)
	{
		return  checkerBoard.color;
	}
	return checkerBoard.additionalColor;
}
Color getRefractedColor(Triangle tr, Ray r, double t)
{
	Point P = getPoint(r.O,r.d,t);
	Vector i = Vector(r.O,P);
	double nbyn = 1/tr.refractiveIndex;
	Vector AC = Vector(tr.A,tr.C);
	Vector AB = Vector(tr.A,tr.B);
	Vector n1 = AB.crossProduct(AC);
	Vector n2 = AC.crossProduct(AB);
	n1.normalize();
	n2.normalize();
	Vector n;
	if(n1.dotProduct(i) < 0)
	{
		n = n2;
	}else
	{
		n = n1;
	}
	n.normalize();
	i.normalize();
	Vector fp = i.minus(n.multiply(n.dotProduct(i)));
	fp = fp.multiply(nbyn);
	Vector ti = i;
	ti.normalize();
	ti = ti.negative();
	double cosThetaI = ti.dotProduct(n);
	double thatFuckingConstant = 1-cosThetaI*cosThetaI;
	thatFuckingConstant = thatFuckingConstant*nbyn*nbyn;
	thatFuckingConstant = 1 - thatFuckingConstant;
	thatFuckingConstant = sqrt(thatFuckingConstant);
	Vector fn = n.multiply(thatFuckingConstant);
	Vector f = fp.add(fn);
	f.normalize();
	Point EP = getPoint(P,f,EPSILON);
	Ray refractedRay = Ray(EP,f);
	Color color = trace(refractedRay,3);
	return color;
}
Color trace(Ray r,int depth)
{
	if(depth == 0)
	{
		return Color(BLACK);
	}
	Color c = Color(0,0,0);
	int nS = numberOfSpCyTr[SPHERE],nC= numberOfSpCyTr[CYLINDER],nT= numberOfSpCyTr[TRIANGLE];
	double minDistance = INFINITY;
	int closestObject = -1;
	int indexOfClosestSphere = -1;
	int indexOfClosestCylinder = -1;
	int indexOfClosestTrianlge = -1;
	//check for sphere
	for(int i = 0 ; i < nS; i++)
	{
		double distance =intersectsWith (r,spheres[i]);
		if(distance != -1)
		{
			if(distance < minDistance)
			{
				minDistance = distance;
				indexOfClosestSphere = i;
				closestObject = SPHERE;
			}
		}
	}
	//check for cylinder
	for(int i = 0 ; i < nC; i++)
	{
		double distance = intersectsWith(r,cylinders[i]);
		if(distance != -1)
		{
			//cout<<distance<<endl;
			if(distance < minDistance)
			{
				minDistance = distance;
				indexOfClosestCylinder = i;
				closestObject = CYLINDER;
			}
		}
	}
	//check with triangle
	for(int i = 0 ; i < nT; i++)
	{
		double distance = intersectsWith(r,triangles[i]);
		if(distance != -1)
		{
			if(distance < minDistance)
			{
				minDistance = distance;
				indexOfClosestTrianlge = i;
				closestObject = TRIANGLE;
			}
		}
	}
	double distance = intersectsWith(r,checkerBoard);
	switch(closestObject)
	{
	case -1:
		{
			if (distance > 0)
			{
				Vector n = Vector(0,0,1);
				n.normalize();
				Point P = getPoint(r.O,r.d,distance);
				Vector OP = Vector(r.O,P);
				Vector LR =  OP.minus(n.multiply(OP.dotProduct(n)*2));
				LR.normalize();
				Point EP = getPoint(P,LR,EPSILON);
				Ray reflectedRay = Ray(EP,LR);
				Color rc = trace(reflectedRay,depth -1 );
				rc.multiply(checkerBoard.reflectionCoefficient);
				c = getColorAtDistance(r,distance);
				c.multiply((1-checkerBoard.reflectionCoefficient));
				c.plus(rc);
			}
			break;
		}
	case SPHERE:
		{
			Point P = getPoint(r.O,r.d,minDistance);
			Vector OP = Vector(r.O,P);
			Vector n = Vector(P,spheres[indexOfClosestSphere].center);
			n = n.negative();
			n.normalize();
			Vector PR = OP.minus(n.multiply(OP.dotProduct(n)*2));
			PR.normalize();
			Point EPoint = getPoint(P,PR,0.5);
			Ray reflectedRay = Ray(EPoint,PR);
			Color reflectedTrace = trace(reflectedRay,depth-1);
			reflectedTrace.multiply(spheres[indexOfClosestSphere].reflectionCoefficient);
			c = getPhongLightColor(spheres[indexOfClosestSphere],r,minDistance);
			c.plus(reflectedTrace);
			break;
		}
	case CYLINDER:
		{
			Point P = getPoint(r.O,r.d,minDistance);
			Vector OP = Vector(r.O,P);
			Point C = Point(cylinders[indexOfClosestCylinder].xCenter,cylinders[indexOfClosestCylinder].zCenter,P.z);
			Vector PC = Vector(P,C);
			Vector n = PC.negative();
			n.normalize();
			Vector PR = OP.minus(n.multiply(OP.dotProduct(n)*2));
			PR.normalize();
			Point EPoint = getPoint(P,PR,EPSILON);
			Ray reflectedRay = Ray(EPoint,PR);
			Color reflectedTrace = trace(reflectedRay,depth-1);
			reflectedTrace.multiply(cylinders[indexOfClosestCylinder].reflectionCoefficient);
			c  = getPhongLightColor(cylinders[indexOfClosestCylinder],r,minDistance);
			c.plus(reflectedTrace);
			break;
		}
	case TRIANGLE:
		{
			Triangle trr = triangles[indexOfClosestTrianlge];
			Point P = getPoint(r.O,r.d,minDistance);
			Vector OP = Vector(r.O,P);
			Vector AC = Vector(trr.A,trr.C);
			Vector AB = Vector(trr.A,trr.B);
			Vector n1 = AB.crossProduct(AC);
			Vector n2 = AC.crossProduct(AB);
			n1.normalize();
			n2.normalize();
			Vector n;
			if(n1.dotProduct(OP) < 0)
			{
				n = n2;
			}else
			{
				n = n1;
			}
			Vector PR = OP.minus(n.multiply(OP.dotProduct(n)*2));
			PR.normalize();
			Point EPoint = getPoint(P,PR,EPSILON);
			Ray reflectedRay = Ray(EPoint,PR);
			Color reflectedTrace = trace(reflectedRay,depth-1);
			reflectedTrace.multiply(trr.reflectionCoefficient);
			//if(depth == reccursionDepth)
			//{
				c = getRefractedColor(trr,r,minDistance);
				c.multiply(1- trr.reflectionCoefficient-trr.ambientCoefficient);
				Color k = trr.color;
				k.multiply(trr.ambientCoefficient);
				c.plus(k);
			//}else
			//{
				//c = getPhongLightColor(trr,r,minDistance);
		//	}
			c.plus(reflectedTrace);
			break;
		}
	}
	return c;
}

void traceRay()
{
	FILE * fp;
	fp = fopen("log.txt","w");
	printf("processing....\n");
	Point eye = Point(cameraX,cameraY,cameraZ);
	Point lookAt = Point(cameraRadius*cos(cameraAngle)+cameraX, cameraRadius*sin(cameraAngle)+cameraY,cameraZ+cameraRadius*tan(cameraAngleV));
	
	Vector u = Vector(sinf(screw),0,cosf(screw));
	u.normalize();
	Point uPoint = getPoint(eye,u,1.0);
	Vector up = Vector(eye,uPoint);
	up.normalize();
	
	Vector lookingDirection = Vector(eye,lookAt);
	lookingDirection.normalize();
	Vector orthogonalToUpAndLookingDirection = up.crossProduct(lookingDirection);
	orthogonalToUpAndLookingDirection.normalize();

	double N = 500;
	int W, H;
	W = H = pixels;
	double scale = 1;
	Point P = getPoint(eye,lookingDirection,N);
	Point Q = getPoint(P,orthogonalToUpAndLookingDirection,W*scale/2);
	Point R = getPoint(Q,up.negative(),H*scale/2);
	Color* image = new Color[W*H];
	int currentPixel;
	debug = new Point[W*H];
	
	for(int h = 0 ; h < H; h++)
	{
		Point S = getPoint(R,orthogonalToUpAndLookingDirection.negative(),h*scale);
		for(int w = 0 ; w < W ; w++)
		{
			Point x = getPoint(S,up,w*scale);
			Ray rayToBetraced = Ray(eye,x);
			currentPixel = w*H+h;
			image[currentPixel] = trace(rayToBetraced,reccursionDepth);
		}
	}
	saveImage("output.bmp",W,H,72,image);
	printf("done\n");
	fclose(fp);
}
#endif