#ifndef _OBJECT_READER
#define _OBJECT_READER
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"sphere.h"
#include"cylinder.h"
#include"triangle.h"
#include"checkerboard.h"
#include"light.h"
int readRecDepth()
{
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	int recDepth = 50;
	if(fp == NULL)
	{
		printf("Error opening file");
	}else
	{
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"recDepth") == 0)
			{
				done = fscanf(fp, "%d",&recDepth);
				break;
			}
		}while(done != EOF);
		fclose(fp);
	}
	return recDepth;
}
int readPixels()
{
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	int pixels = 50;
	if(fp == NULL)
	{
		printf("Error opening file");
	}else
	{
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"pixels") == 0)
			{
				done = fscanf(fp, "%d",&pixels);
				break;
			}
		}while(done != EOF);
		fclose(fp);
	}
	return pixels;
}
int getNumberOfLights()
{
	int result = 0;
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	if(fp == NULL)
	{
		printf("Error opening file");
	}else
	{
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"light") == 0)
			{
				result++;
			}
		}while(done != EOF);
		fclose(fp);
	}
	return result;
}
int* getNumberOfSphereCylinderTriangle()
{
	int *result = new int[3];
	result[0] = result[1] = result[2] = 0;
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	if(fp == NULL)
	{
		printf("Error opening file");
	}else
	{
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"SPHERE") == 0)
			{
				result[0]++;
			}
			if(strcmp(word,"CYLINDER") == 0)
			{
				result[1]++;
			}
			if(strcmp(word,"TRIANGLE") == 0)
			{
				result[2]++;
			}
		}while(done != EOF);
		fclose(fp);
	}
	return result;
}

Sphere* readSpheres(int numberOfSpheres)
{
	
	Sphere* result = new Sphere[numberOfSpheres];
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	
	if(fp == NULL)
	{
		
		printf("Error opening file");
	}else
	{
		int count = 0 ;
		
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"SPHERE") == 0)
			{
				printf(">>>> found an sphere\n\n");
				result[count].refractiveIndex = -1;
				done = fscanf(fp, "%s",word);
				do{
					if(strcmp(word,"center")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						result[count].center = Point(x,z,y);
					}else if(strcmp(word,"radius")==0)
					{
						double r;

						fscanf(fp,"%lf",&r);
						
						result[count].radius = r;
					}else if(strcmp(word,"color")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						printf(">>>>>>>>%lf,%lf,%lf<<<<<\n\n",x,y,z);
						result[count].color = Color(x*255,y*255,z*255);
					}else if(strcmp(word,"ambCoeff")==0)
					{
						double ac;
						fscanf(fp,"%lf",&ac);
						result[count].ambientCoefficient = ac;
					}else if(strcmp(word,"difCoeff")==0)
					{
						double dc;
						fscanf(fp,"%lf",&dc);
						result[count].diffiuseCoefficient = dc;	
					}else if(strcmp(word,"refCoeff")==0)
					{
						double rc;
						fscanf(fp,"%lf",&rc);
						result[count].reflectionCoefficient = rc;
					}else if(strcmp(word,"specCoeff")==0)
					{
						double sc;
						fscanf(fp,"%lf",&sc);
						result[count].specularCoefficient = sc;
					}else if(strcmp(word,"specExp")==0)
					{
						double pc;
						fscanf(fp,"%lf",&pc);
						result[count].phongCoefficient = pc;
					}else if(strcmp(word,"refractiveIndex")==0)
					{
						double ri;
						fscanf(fp,"%lf",&ri);
						result[count].refractiveIndex = ri;
					}
					done = fscanf(fp, "%s",word);
				}while(strcmp(word,"objEnd") != 0);
				count++;
			}
		}while(done != EOF);
		fclose(fp);
	}
	return result;
}
CheckerBoard readCheckerBoard()
{
	
	CheckerBoard checkerBoard;
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	
	if(fp == NULL)
	{
		
		printf("Error opening file");
	}else
	{
		int count = 0 ;
		
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"CHECKERBOARD") == 0)
			{
				printf(">>>> found an CHECKERBOARD\n\n");
				done = fscanf(fp, "%s",word);
				do{
					if(strcmp(word,"colorOne")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						printf(">>>>>>>>%lf,%lf,%lf<<<<<\n\n",x,y,z);
						checkerBoard.color = Color(x*255,y*255,z*255);
					}
					else if(strcmp(word,"colorTwo")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						printf(">>>>>>>>%lf,%lf,%lf<<<<<\n\n",x,y,z);
						checkerBoard.additionalColor = Color(x*255,y*255,z*255);
					}else if(strcmp(word,"ambCoeff")==0)
					{
						double ac;
						fscanf(fp,"%lf",&ac);
						checkerBoard.ambientCoefficient = ac;
					}else if(strcmp(word,"difCoeff")==0)
					{
						double dc;
						fscanf(fp,"%lf",&dc);
						checkerBoard.diffiuseCoefficient = dc;	
					}else if(strcmp(word,"refCoeff")==0)
					{
						double rc;
						fscanf(fp,"%lf",&rc);
						checkerBoard.reflectionCoefficient = rc;
					}else if(strcmp(word,"specCoeff")==0)
					{
						double sc;
						fscanf(fp,"%lf",&sc);
						checkerBoard.specularCoefficient = sc;
					}else if(strcmp(word,"specExp")==0)
					{
						double pc;
						fscanf(fp,"%lf",&pc);
						checkerBoard.phongCoefficient = pc;
					}else if(strcmp(word,"refractiveIndex")==0)
					{
						double ri;
						fscanf(fp,"%lf",&ri);
						checkerBoard.refractiveIndex = ri;
					}
					done = fscanf(fp, "%s",word);
				}while(strcmp(word,"objEnd") != 0);
				count++;
			}
		}while(done != EOF);
		fclose(fp);
	}
	checkerBoard.size = 20;
	checkerBoard.range = 1000;
	return checkerBoard;
}
Cylinder* readCylinders(int numberOfCylinder)
{
	
	Cylinder* result = new Cylinder[numberOfCylinder];
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	
	if(fp == NULL)
	{
		
		printf("Error opening file");
	}else
	{
		int count = 0 ;
		
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"CYLINDER") == 0)
			{
				printf(">>>> found an cylinder\n\n");
				result[count].refractiveIndex = -1;
				done = fscanf(fp, "%s",word);
				do{
					if(strcmp(word,"xCenter")==0)
					{
						double x;
						fscanf(fp,"%lf",&x);
						result[count].xCenter = x;
					}else if(strcmp(word,"zCenter")==0)
					{
						double z;

						fscanf(fp,"%lf",&z);
						
						result[count].zCenter = z;
					}else if(strcmp(word,"radius")==0)
					{
						double r;

						fscanf(fp,"%lf",&r);
						
						result[count].radius = r;
					}else if(strcmp(word,"yMin")==0)
					{
						double yMin;

						fscanf(fp,"%lf",&yMin);
						
						result[count].yMin = yMin;
					}else if(strcmp(word,"yMax")==0)
					{
						double yMax;

						fscanf(fp,"%lf",&yMax);
						
						result[count].yMax = yMax;
					}else if(strcmp(word,"color")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						printf(">>>>>>>>%lf,%lf,%lf<<<<<\n\n",x,y,z);
						result[count].color = Color(x*255,y*255,z*255);
					}else if(strcmp(word,"ambCoeff")==0)
					{
						double ac;
						fscanf(fp,"%lf",&ac);
						result[count].ambientCoefficient = ac;
					}else if(strcmp(word,"difCoeff")==0)
					{
						double dc;
						fscanf(fp,"%lf",&dc);
						result[count].diffiuseCoefficient = dc;	
					}else if(strcmp(word,"refCoeff")==0)
					{
						double rc;
						fscanf(fp,"%lf",&rc);
						result[count].reflectionCoefficient = rc;
					}else if(strcmp(word,"specCoeff")==0)
					{
						double sc;
						fscanf(fp,"%lf",&sc);
						result[count].specularCoefficient = sc;
					}else if(strcmp(word,"specExp")==0)
					{
						double pc;
						fscanf(fp,"%lf",&pc);
						result[count].phongCoefficient = pc;
					}else if(strcmp(word,"refractiveIndex")==0)
					{
						double ri;
						fscanf(fp,"%lf",&ri);
						result[count].refractiveIndex = ri;
					}
					done = fscanf(fp, "%s",word);
				}while(strcmp(word,"objEnd") != 0);
				count++;
			}
		}while(done != EOF);
		fclose(fp);
	}
	return result;
}
Triangle* readTriangles(int numberOfTriangle)
{
	
	Triangle* result = new Triangle[numberOfTriangle];
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	
	if(fp == NULL)
	{
		
		printf("Error opening file");
	}else
	{
		int count = 0 ;
		
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"TRIANGLE") == 0)
			{
				printf(">>>> found an TRIANGLE\n\n");
				result[count].refractiveIndex = -1;
				done = fscanf(fp, "%s",word);
				do{
					if(strcmp(word,"a")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						result[count].A = Point(x,z,y);
					}else if(strcmp(word,"b")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						result[count].B = Point(x,z,y);
					}else if(strcmp(word,"c")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						result[count].C = Point(x,z,y);
					}else if(strcmp(word,"color")==0)
					{
						double x,y,z;
						fscanf(fp,"%lf%lf%lf",&x,&y,&z);
						printf(">>>>>>>>%lf,%lf,%lf<<<<<\n\n",x,y,z);
						result[count].color = Color(x*255,y*255,z*255);
					}else if(strcmp(word,"ambCoeff")==0)
					{
						double ac;
						fscanf(fp,"%lf",&ac);
						result[count].ambientCoefficient = ac;
					}else if(strcmp(word,"difCoeff")==0)
					{
						double dc;
						fscanf(fp,"%lf",&dc);
						result[count].diffiuseCoefficient = dc;	
					}else if(strcmp(word,"refCoeff")==0)
					{
						double rc;
						fscanf(fp,"%lf",&rc);
						result[count].reflectionCoefficient = rc;
					}else if(strcmp(word,"specCoeff")==0)
					{
						double sc;
						fscanf(fp,"%lf",&sc);
						result[count].specularCoefficient = sc;
					}else if(strcmp(word,"specExp")==0)
					{
						double pc;
						fscanf(fp,"%lf",&pc);
						result[count].phongCoefficient = pc;
					}else if(strcmp(word,"refractiveIndex")==0)
					{
						double ri;
						fscanf(fp,"%lf",&ri);
						result[count].refractiveIndex = ri;
					}
					done = fscanf(fp, "%s",word);
				}while(strcmp(word,"objEnd") != 0);
				count++;
			}
		}while(done != EOF);
		fclose(fp);
	}
	return result;
}
Light* readLightPositions(int n)
{
	
	Light* result = new Light[n];
	FILE * fp;
	fp = fopen("Spec.txt","r");
	char word[100];
	char done;
	
	if(fp == NULL)
	{
		
		printf("Error opening file");
	}else
	{
		int count = 0 ;
		do
		{
			done = fscanf(fp, "%s",word);
			if(strcmp(word,"light") == 0)
			{
				double x,y,z;
				done = fscanf(fp,"%lf%lf%lf",&x,&y,&z);
				result[count].x = x;
				result[count].y = z;
				result[count].z = y;
				count++;
				if(count == n)
				{
					break;
				}
			}
		}while(done != EOF);
		fclose(fp);
	}
	return result;
}
#endif