#ifndef _POINT
#define _POINT
class Point
{
	public:
		double x;
		double y; 
		double z;
		Point();
		Point(double,double,double);
		Point getPoint(GLdouble x, GLdouble y , GLdouble z)
		{
			Point p;
			p.x = x;
			p.y = y;
			p.z = z;
			return p;
		}
};
Point::Point()
{
	x = 0;
	y = 0;
	z = 0;
}
Point::Point(double a,double b, double c)
{
	x = a;
	y = b;
	z = c;
}
#endif